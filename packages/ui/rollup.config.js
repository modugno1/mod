import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import external from 'rollup-plugin-peer-deps-external'
import babel from '@rollup/plugin-babel'
import packageJson from './package.json'

export default {
  input: 'src/index.js',
  plugins: [
    babel({ exclude: 'node_modules/**' }),
    external(),
    resolve(),
    commonjs(),
    terser()
  ],
  output: [
    {
      file: packageJson.main,
      format: 'cjs',
      sourcemap: true
    }
  ]
}
