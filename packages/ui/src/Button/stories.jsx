import { log } from '@mod/utils'
import Button from '.'

export default {
  title: 'Button',
  component: Button,
}

export const Base = () => <Button onClick={() => log('olá mundo')}>Button</Button>
