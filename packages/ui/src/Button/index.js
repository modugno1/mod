import styled from 'styled-components'
import { palette } from '@mod/tokens'
import { em } from '@mod/utils'

const Button = styled.button`
  background-color: ${palette.primary};
  color: ${palette.contrast};
  border: none;
  font-size: ${em(14)}rem;
`

export default Button
