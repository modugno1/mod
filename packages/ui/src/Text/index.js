import styled from 'styled-components'
import { spacing } from '@mod/tokens'

const Text = styled.p`
  margin: ${spacing.medium} ${spacing.small};
`

export default Text
