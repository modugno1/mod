import React from 'react'

export enum Genre {
  ma_le = 'Masculino',
  fe_male = 'Feminino'
}

type Props = {
  genre: Genre
}

const Input = (props: Props) => <div>{props.genre}</div>

export default Input
