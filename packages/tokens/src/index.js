export default {
  palette: {
    primary: '#000',
    secondary: '#333',
    contrast: '#fff'
  },
  spacing: {
    small: '1rem',
    medium: '2rem',
    large: '3rem'
  }
}
